#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "Resources.h"
#include "MenuButton.h"
#include "Board.h"
#include "Text.h"
#include "PlayerIndicator.h"

enum class GameState
{
	PLACING,
	MOVING,
	REMOVING,
	GAMEOVER,
};

class Game
{
private:
	// game settings
	const int numberOfCoinsPerPlayer = 9;
	const int minNumberOfCoins = 3;
	const bool flyingModeEnabled = true;

	const int numberOfPlayers = 2;
	int currentPlayerIndex = -1;
	GameState state = GameState::GAMEOVER;
	Board *board;
	std::vector<MenuButton*> buttons; // (holds menu buttons)
	PlayerIndicator playerIndicator;
	sf::RectangleShape background;
	sf::RectangleShape rectWhiteScore;
	sf::RectangleShape rectBlackScore;
	Text textTitle;
	Text textStatus;
	Text textWhiteScore;
	Text textBlackScore;
	sf::Sound soundCoinSelected;
	sf::Sound soundCoinMoved;
	sf::Sound soundCoinRemoved;
	sf::Sound soundPointPressed;
	sf::Sound soundButtonPressed;
	sf::Sound soundGameOver;
	sf::Sound soundLineformed;

public:
	Game();

	void init();
	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	void reset();
	void changeState(GameState newState);
	void setBackground(sf::Texture &texture);
	void advanceCurrentPlayerIndex();
	void updatePlayerIndicator(int playerIndex);
	void updatePlayerScore(int playerIndex, int score);
	void updateScores();
	void decrementPlayerScore(int playerIndex);

private:
	void processStates();
	void processMenuButtons(sf::RenderWindow &window);
};
