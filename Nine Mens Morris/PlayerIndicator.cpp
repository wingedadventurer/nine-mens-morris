#include "PlayerIndicator.h"



PlayerIndicator::PlayerIndicator()
{
	init();
}

PlayerIndicator::~PlayerIndicator()
{
}

void PlayerIndicator::init()
{
	textureWhiteIndicator = &Resources::get().texture(TextureResourceType::WHITE_INDICATOR);
	textureBlackIndicator = &Resources::get().texture(TextureResourceType::BLACK_INDICATOR);
	rect.setSize(sf::Vector2f(180.0f, 40.0f));
	rect.setTexture(&Resources::get().texture(TextureResourceType::WHITE_INDICATOR));
	setPosition(sf::Vector2f(210.0f, -40.0f));
}

void PlayerIndicator::update(sf::RenderWindow & window, int delta)
{
	// smooth movement
	float weight = moveLerpWeight * (delta * 0.001f);
	weight = weight > 1.0f ? 1.0f : weight;
	sf::Vector2f distance = targetPosition - position;
	setPositionSmooth(getPosition() + distance * weight);
}

void PlayerIndicator::draw(sf::RenderWindow & window)
{
	window.draw(rect);
}

sf::Vector2f PlayerIndicator::getPosition()
{
	return position;
}

void PlayerIndicator::setPlayerTexture(int playerIndex)
{
	switch (playerIndex)
	{
	case 0:
		rect.setTexture(textureWhiteIndicator);
		break;
	case 1:
		rect.setTexture(textureBlackIndicator);
		break;
	}
}

void PlayerIndicator::setPosition(sf::Vector2f position)
{
	this->position = position;
	rect.setPosition(position);
	targetPosition = position;
}

void PlayerIndicator::setPositionSmooth(sf::Vector2f position)
{
	this->position = position;
	rect.setPosition(position);
}

void PlayerIndicator::moveTo(sf::Vector2f position, bool smooth)
{
	if (smooth) {
		targetPosition = position;
	}
	else {
		setPosition(position);
	}
}
