#pragma once

#include <vector>
#include <iostream>
#include "Point.h"
#include "Coin.h"

class Line
{
private:
	bool disabled = false;
	std::vector<Point *> points;

public:
	Line();

	bool isCompleted();
	int getCompletedPlayerIndex();
	void addPoint(Point *point);
	void printPoints();
	bool isDisabled();
	void disable();
	void enable();
	void refresh();
	void selectCoins();
	void disableCoins();
};

