#include "Resources.h"

Resources & Resources::get()
{
	static Resources instance;
	return instance;
}

Resources::Resources()
{
	loadAllResources();
}

void Resources::loadAllResources()
{
	// textures
	textures[TextureResourceType::BACKGROUND]			.loadFromFile("./res/textures/nmm_background.png");
	textures[TextureResourceType::BOARD]				.loadFromFile("./res/textures/nmm_board.png");
	textures[TextureResourceType::BUTTON_NORMAL]		.loadFromFile("./res/textures/nmm_button-normal.png");
	textures[TextureResourceType::BUTTON_HOVERED]		.loadFromFile("./res/textures/nmm_button-hover.png");
	textures[TextureResourceType::BUTTON_PRESSED]		.loadFromFile("./res/textures/nmm_button-pressed.png");
	textures[TextureResourceType::POINT_NORMAL]			.loadFromFile("./res/textures/nmm_point-normal.png");
	textures[TextureResourceType::POINT_HOVERED]		.loadFromFile("./res/textures/nmm_point-hover.png");
	textures[TextureResourceType::POINT_PRESSED]		.loadFromFile("./res/textures/nmm_point-pressed.png");
	textures[TextureResourceType::POINT_DISABLED]		.loadFromFile("./res/textures/nmm_point-disabled.png");
	textures[TextureResourceType::COIN_WHITE_NORMAL]	.loadFromFile("./res/textures/nmm_coin-white-normal.png");
	textures[TextureResourceType::COIN_WHITE_HOVERED]	.loadFromFile("./res/textures/nmm_coin-white-hover.png");
	textures[TextureResourceType::COIN_WHITE_PRESSED]	.loadFromFile("./res/textures/nmm_coin-white-pressed.png");
	textures[TextureResourceType::COIN_WHITE_DISABLED]	.loadFromFile("./res/textures/nmm_coin-white-disabled.png");
	textures[TextureResourceType::COIN_BLACK_NORMAL]	.loadFromFile("./res/textures/nmm_coin-black-normal.png");
	textures[TextureResourceType::COIN_BLACK_HOVERED]	.loadFromFile("./res/textures/nmm_coin-black-hover.png");
	textures[TextureResourceType::COIN_BLACK_PRESSED]	.loadFromFile("./res/textures/nmm_coin-black-pressed.png");
	textures[TextureResourceType::COIN_BLACK_DISABLED]	.loadFromFile("./res/textures/nmm_coin-black-disabled.png");
	textures[TextureResourceType::COIN_SELECTED]		.loadFromFile("./res/textures/nmm_coin-selected.png");
	textures[TextureResourceType::WHITE_INDICATOR]		.loadFromFile("./res/textures/nmm_white-indicator.png");
	textures[TextureResourceType::BLACK_INDICATOR]		.loadFromFile("./res/textures/nmm_black-indicator.png");
	textures[TextureResourceType::WHITE_SCORE]			.loadFromFile("./res/textures/nmm_white-score.png");
	textures[TextureResourceType::BLACK_SCORE]			.loadFromFile("./res/textures/nmm_black-score.png");
	
	// sounds
	sounds[SoundResourceType::BUTTON_PRESSED]			.loadFromFile("./res/audio/sfx/button-pressed.wav");
	sounds[SoundResourceType::COIN_SELECTED]			.loadFromFile("./res/audio/sfx/coin-selected.wav");
	sounds[SoundResourceType::COIN_REMOVED]				.loadFromFile("./res/audio/sfx/coin-removed.wav");
	sounds[SoundResourceType::COIN_MOVED]				.loadFromFile("./res/audio/sfx/coin-moved.wav");
	sounds[SoundResourceType::POINT_PRESSED]			.loadFromFile("./res/audio/sfx/point-pressed.wav");
	sounds[SoundResourceType::GAME_OVER]				.loadFromFile("./res/audio/sfx/game-over.wav");
	sounds[SoundResourceType::LINE_FORMED]				.loadFromFile("./res/audio/sfx/line-formed.wav");

	// fonts
	fonts[FontResourceType::MAIN]						.loadFromFile("./res/fonts/SourceSansPro-Bold.ttf");

	// colors
	colors[ColorResourceType::WHITE_LIGHT]	= sf::Color(168, 168, 168);
	colors[ColorResourceType::WHITE_DARK]	= sf::Color(122, 122, 122);
	colors[ColorResourceType::BLACK_LIGHT]	= sf::Color(66, 66, 66);
	colors[ColorResourceType::BLACK_DARK]	= sf::Color(48, 48, 48);
}

sf::Texture & Resources::texture(TextureResourceType type)
{
	return textures[type];
}

sf::SoundBuffer & Resources::sound(SoundResourceType type)
{
	return sounds[type];
}

sf::Font & Resources::font(FontResourceType type)
{
	return fonts[type];
}

sf::Color & Resources::color(ColorResourceType type)
{
	return colors[type];
}
