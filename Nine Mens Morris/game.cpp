#include "Game.h"

Game::Game()
{
	init();
}

void Game::init()
{
	board = new Board(sf::Vector2f(100.0f, 50.0f), numberOfCoinsPerPlayer);

	// set sounds
	soundCoinSelected.setBuffer(Resources::get().sound(SoundResourceType::COIN_SELECTED));
	soundCoinMoved.setBuffer(Resources::get().sound(SoundResourceType::COIN_MOVED));
	soundCoinRemoved.setBuffer(Resources::get().sound(SoundResourceType::COIN_REMOVED));
	soundPointPressed.setBuffer(Resources::get().sound(SoundResourceType::POINT_PRESSED));
	soundButtonPressed.setBuffer(Resources::get().sound(SoundResourceType::BUTTON_PRESSED));
	soundGameOver.setBuffer(Resources::get().sound(SoundResourceType::GAME_OVER));
	soundLineformed.setBuffer(Resources::get().sound(SoundResourceType::LINE_FORMED));

	// setup background
	background.setSize(sf::Vector2f(600.0f, 600.0f));
	setBackground(Resources::get().texture(TextureResourceType::BACKGROUND));

	// create menu buttons
	buttons.push_back(new MenuButton(sf::Vector2f(20.0f, 520.0f), "Start"));
	buttons.push_back(new MenuButton(sf::Vector2f(460.0f, 520.0f), "Quit"));

	// setup score textures
	rectWhiteScore.setSize(sf::Vector2f(80.0f, 80.0f));
	rectWhiteScore.setPosition(sf::Vector2f(-10.0f, 20.0f));
	rectWhiteScore.setTexture(&Resources::get().texture(TextureResourceType::WHITE_SCORE));
	rectBlackScore.setSize(sf::Vector2f(80.0f, 80.0f));
	rectBlackScore.setPosition(sf::Vector2f(530.0f, 20.0f));
	rectBlackScore.setTexture(&Resources::get().texture(TextureResourceType::BLACK_SCORE));

	// setup title and status texts
	textTitle.setPosition(sf::Vector2f(300.0f, 480.0f));
	textTitle.setText("Nine Men's Morris v1.0");
	textStatus.setPosition(sf::Vector2f(300.0f, 550.0f));
	textStatus.setText("By: Karlo Koscal");

	// setup score texts
	textWhiteScore.setFontSize(42);
	textWhiteScore.setColor(Resources::get().color(ColorResourceType::WHITE_DARK));
	textWhiteScore.setPosition(sf::Vector2f(32.0f, 60.0f));
	textWhiteScore.setText("");
	textBlackScore.setFontSize(42);
	textBlackScore.setColor(Resources::get().color(ColorResourceType::BLACK_DARK));
	textBlackScore.setPosition(sf::Vector2f(564.0f, 60.0f));
	textBlackScore.setText("");

	// normalize volumes
	soundCoinMoved.setVolume(20);
	soundButtonPressed.setVolume(40);
	soundPointPressed.setVolume(80);
}

void Game::update(sf::RenderWindow &window, int delta)
{
	processStates();
	processMenuButtons(window);

	// update children
	board->update(window, delta);
	for (auto button : buttons) {
		button->update(window, delta);
	}
	textTitle.update(window, delta);
	textStatus.update(window, delta);
	playerIndicator.update(window, delta);
}

void Game::draw(sf::RenderWindow &window)
{
	window.draw(background);
	window.draw(rectWhiteScore);
	window.draw(rectBlackScore);

	// draw children
	board->draw(window);
	for (auto button : buttons) {
		button->draw(window);
	}
	textTitle.draw(window);
	textStatus.draw(window);
	textWhiteScore.draw(window);
	textBlackScore.draw(window);
	playerIndicator.draw(window);
}

void Game::processStates()
{
	switch (state)
	{
	// -------------------------------------------------------------------------------------------------
	case GameState::PLACING:
		// wait for point selection
		if (board->hasJustSelectedPoint()) {
			// place coin
			board->placeSelectedCoin(board->getJustSelectedPoint());
			soundPointPressed.play();
			decrementPlayerScore(currentPlayerIndex);

			// check if new line has formed
			if (board->hasPlayerLine(currentPlayerIndex)) {
				// remove enemy coin
				changeState(GameState::REMOVING);
				break;
			}

			// advance
			advanceCurrentPlayerIndex();

			// check count of unplaced coins
			if (board->hasUnplacedCoin()) {
				// place again
				changeState(GameState::PLACING);
				break;
			}
			else {
				// all coins placed, proceed
				changeState(GameState::MOVING);
				break;
			}
		}
		break;
	// -------------------------------------------------------------------------------------------------
	case GameState::MOVING:
		// wait for coin selection
		if (board->hasJustSelectedCoin()) {
			soundCoinSelected.play();

			if (board->hasSelectedCoin()) {
				// undo move intention
				board->getSelectedCoin()->deselect();
				changeState(GameState::MOVING);
			}
			else {
				// select new coin
				board->disableAllCoins();
				board->getJustSelectedCoin()->enable(); // allow for undo
				board->setSelectedCoin(board->getJustSelectedCoin());
				board->getJustSelectedCoin()->select();

				// check if eligible for flying mode (if flying is turned on)
				if (flyingModeEnabled && board->getNumberOfPlayerUnremovedCoins(currentPlayerIndex) == minNumberOfCoins) {
					board->enableRemainingPoints();
				}
				else {
					board->getJustSelectedCoin()->getLinkedPoint()->enableFreeConnectedPoints();
				}

				textStatus.setText("Move your coin");
			}
		}
		// wait for point selection
		else if (board->hasJustSelectedPoint()) {
			if (board->hasSelectedCoin()) { // just in case
				// move
				board->getSelectedCoin()->deselect();
				board->moveSelectedCoinToPoint(board->getJustSelectedPoint());
				board->disableAllPoints();
				board->disableAllCoins();
				board->refreshLines();
				soundCoinMoved.play();

				// check lines
				if (board->hasPlayerLine(currentPlayerIndex)) {
					changeState(GameState::REMOVING);
					break;
				}


				// see if enemy player can move
				board->enablePlayerPlacedCoins(1 - currentPlayerIndex);
				board->disableUnmovableCoins();
				if (!board->hasEnabledPlayerCoin(1 - currentPlayerIndex)) {
					changeState(GameState::GAMEOVER);
					break;
				}

				advanceCurrentPlayerIndex();

				// repeat
				changeState(GameState::MOVING);
				break;
			}
		}

		break;
	// -------------------------------------------------------------------------------------------------
	case GameState::REMOVING:
		if (board->hasJustSelectedCoin()) {
			// remove enemy coin
			board->getJustSelectedCoin()->remove();
			board->deselectAllCoins();
			soundCoinRemoved.play();

			// check if it's game over (count less than minimum)
			if (board->getNumberOfPlayerUnremovedCoins(1 - currentPlayerIndex) < minNumberOfCoins) {
				changeState(GameState::GAMEOVER);
				break;
			}

			// see if enemy player can move
			board->enablePlayerPlacedCoins(1 - currentPlayerIndex);
			board->disableUnmovableCoins();
			if (!board->hasEnabledPlayerCoin(1 - currentPlayerIndex)) {
				changeState(GameState::GAMEOVER);
				break;
			}

			advanceCurrentPlayerIndex();

			// check if we are still placing coins
			if (board->hasUnplacedCoin()) {
				changeState(GameState::PLACING);
				break;
			}
			else {
				changeState(GameState::MOVING);
				break;
			}
		}
		break;
	// -------------------------------------------------------------------------------------------------
	case GameState::GAMEOVER:
		break;
	}
}

void Game::processMenuButtons(sf::RenderWindow &window)
{
	for (auto button : buttons) {
		// ignore if button not pressed
		if (!button->isJustPressed()) { continue; }

		soundButtonPressed.play();

		// make decision based on button text
		std::string text = button->getText().toAnsiString();
		if (text == "Reset") {
			reset();
		}
		else if (text == "Quit") {
			window.close();
		}
		else if (text == "Start") {
			button->setText("Reset");
			reset();
		}
	}
}

void Game::changeState(GameState newState)
{
	switch (newState)
	{
	case GameState::PLACING:
		board->disableAllCoins();
		board->unlinkDisabledCoins();
		board->enableRemainingPoints();
		board->refreshLines();
		board->selectUnplacedCoin(currentPlayerIndex);
		textStatus.setText("Place your coin");
		break;

	case GameState::MOVING:
		if (state == GameState::REMOVING) {
			board->unlinkDisabledCoins();
			board->refreshLines();
		}

		board->disableAllPoints();
		board->setSelectedCoin(nullptr);
		board->enablePlayerPlacedCoins(currentPlayerIndex);
		updateScores();

		// check if flying mode is on, and enable/disable coins appropriately
		if (!(flyingModeEnabled && board->getNumberOfPlayerUnremovedCoins(currentPlayerIndex) == minNumberOfCoins)) {
			board->disableUnmovableCoins();
		}
		textStatus.setText("Select coin to move");
		break;

	case GameState::REMOVING:
		soundLineformed.play();
		board->disableAllPoints();
		board->selectPlayerLineCoins(currentPlayerIndex); // highlight effect
		board->disablePlayerLines(currentPlayerIndex);
		textStatus.setText("Remove enemy coin");

		// enable enemy coins for removal:
		// you cannot remove coins directly from enemy mills,
		// unless there's no other option
		board->enablePlayerPlacedCoins(1 - currentPlayerIndex); // enable all enemy coins
		board->disablePlayerLineCoins(1 - currentPlayerIndex); // disable enemy line coins
		if (!board->hasEnabledPlayerCoin(1 - currentPlayerIndex)) { // check if any enabled coins are left
			// no available coins after disabling line coins; enabling all again
			board->enablePlayerPlacedCoins(1 - currentPlayerIndex);
		}

		break;

	case GameState::GAMEOVER:
		board->disableAllPoints();
		board->disableAllCoins();
		soundGameOver.play();
		updateScores();

		switch (currentPlayerIndex) {
		case 0:
			textStatus.setText("Player WHITE wins!");
			break;
		case 1:
			textStatus.setText("Player BLACK wins!");
			break;
		}
		break;
	}

	state = newState;
}

void Game::setBackground(sf::Texture &texture)
{
	background.setTexture(&texture);
}

void Game::reset()
{
	board->reset();

	// selecting random player
	currentPlayerIndex = rand() % numberOfPlayers;
	updatePlayerIndicator(currentPlayerIndex);

	// update texts
	textBlackScore.setText(std::to_string(numberOfCoinsPerPlayer));
	textWhiteScore.setText(std::to_string(numberOfCoinsPerPlayer));
	textTitle.setText("");

	changeState(GameState::PLACING);
}

void Game::advanceCurrentPlayerIndex()
{
	currentPlayerIndex++;
	if (currentPlayerIndex >= numberOfPlayers) {
		currentPlayerIndex = 0;
	}

	updatePlayerIndicator(currentPlayerIndex);
}

void Game::updatePlayerIndicator(int playerIndex)
{
	switch (playerIndex)
	{
	case 0:
		playerIndicator.moveTo(sf::Vector2f(100.0f, 0.0f));
		break;
	case 1:
		playerIndicator.moveTo(sf::Vector2f(320.0f, 0.0f));
		break;
	}
	playerIndicator.setPlayerTexture(playerIndex);
}

void Game::updatePlayerScore(int playerIndex, int score)
{
	switch (playerIndex)
	{
	case 0:
		textWhiteScore.setText(std::to_string(score));
		break;
	case 1:
		textBlackScore.setText(std::to_string(score));
		break;
	}
}

void Game::updateScores()
{
	textWhiteScore.setText(std::to_string(board->getNumberOfPlayerUnremovedCoins(0)));
	textBlackScore.setText(std::to_string(board->getNumberOfPlayerUnremovedCoins(1)));
}

void Game::decrementPlayerScore(int playerIndex)
{
	int score;
	switch (playerIndex)
	{
	case 0:
		score = std::stoi(textWhiteScore.getText());
		score--;
		textWhiteScore.setText(std::to_string(score));
		break;
	case 1:
		score = std::stoi(textBlackScore.getText());
		score--;
		textBlackScore.setText(std::to_string(score));
		break;
	}
}
