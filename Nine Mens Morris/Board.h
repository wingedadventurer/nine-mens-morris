#pragma once

#include <vector>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Resources.h"
#include "Point.h"
#include "Coin.h"
#include "Line.h"

class Board
{
private:
	sf::Vector2f position;
	std::vector<Point *> points;
	std::vector<Coin *> coins;
	std::vector<Line *> lines;
	sf::RectangleShape background;
	Coin *selectedCoin = nullptr;
	Coin *justSelectedCoin = nullptr;
	Point *justSelectedPoint = nullptr;
	bool justPlacedCoin = false;

public:
	Board(sf::Vector2f position, int numberOfCoinsPerPlayer);
	
	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	void reset();
	void setup(int numberOfCoinsPerPlayer);
	
	void setBackground(sf::Texture &texture);
	void setPosition(sf::Vector2f position);
	
	// Point-related methods
	void enableAllPoints();
	void disableAllPoints();
	void enableRemainingPoints();
	void unlinkDisabledCoins();
	
	// Coin-related methods
	void selectUnplacedCoin(int playerIndex);
	bool hasUnplacedCoin();
	bool hasJustPlacedCoin();
	void deselectCoin();
	void deselectAllCoins();
	void enablePlayerPlacedCoins(int playerIndex);
	void disableAllCoins();
	void disableUnmovableCoins();
	bool hasSelectedCoin();
	Coin *getSelectedCoin();
	Point *getPointWithSelectedCoin();
	void placeSelectedCoin(Point *point);
	void setSelectedCoin(Coin *coin);
	void moveSelectedCoinToPoint(Point *point);
	int getNumberOfPlayerUnremovedCoins(int playerIndex);
	bool hasEnabledPlayerCoin(int playerIndex);
	
	// Line-related methods
	void refreshLines();
	bool hasLine();
	bool hasPlayerLine(int playerIndex);
	void disablePlayerLines(int playerIndex);
	int getLinePlayerIndex();
	void selectPlayerLineCoins(int playerIndex);
	void disablePlayerLineCoins(int playerIndex);
	void printLines();
	
	// event-like methods
	bool hasJustSelectedCoin();
	Coin *getJustSelectedCoin();
	bool hasJustSelectedPoint();
	Point *getJustSelectedPoint();
};
