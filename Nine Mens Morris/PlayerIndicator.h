#pragma once

#include "Resources.h"
#include <SFML/Graphics.hpp>

class PlayerIndicator
{
private:
	float moveLerpWeight = 10.0f;

	sf::RectangleShape rect;
	sf::Vector2f position;
	sf::Vector2f targetPosition;

	sf::Texture *textureWhiteIndicator;
	sf::Texture *textureBlackIndicator;

public:
	PlayerIndicator();
	~PlayerIndicator();

	void init();
	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	sf::Vector2f getPosition();
	void setPlayerTexture(int playerIndex);
	void setPosition(sf::Vector2f position);
	void setPositionSmooth(sf::Vector2f position);
	void moveTo(sf::Vector2f position, bool smooth = true);
};
