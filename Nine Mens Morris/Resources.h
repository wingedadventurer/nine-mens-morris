#pragma once
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

enum class TextureResourceType
{
	BACKGROUND,
	BOARD,
	BUTTON_NORMAL,
	BUTTON_HOVERED,
	BUTTON_PRESSED,
	POINT_NORMAL,
	POINT_HOVERED,
	POINT_PRESSED,
	POINT_DISABLED,
	COIN_WHITE_NORMAL,
	COIN_WHITE_HOVERED,
	COIN_WHITE_PRESSED,
	COIN_WHITE_DISABLED,
	COIN_BLACK_NORMAL,
	COIN_BLACK_HOVERED,
	COIN_BLACK_PRESSED,
	COIN_BLACK_DISABLED,
	COIN_SELECTED,
	WHITE_INDICATOR,
	BLACK_INDICATOR,
	WHITE_SCORE,
	BLACK_SCORE,
};

enum class SoundResourceType
{
	BUTTON_PRESSED,
	COIN_SELECTED,
	COIN_REMOVED,
	COIN_MOVED,
	POINT_PRESSED,
	GAME_OVER,
	LINE_FORMED,
};

enum class FontResourceType
{
	MAIN,
};

enum class ColorResourceType
{
	WHITE_LIGHT,
	WHITE_DARK,
	BLACK_LIGHT,
	BLACK_DARK,
};

class Resources
{
private:
	std::map<TextureResourceType, sf::Texture> textures;
	std::map<SoundResourceType, sf::SoundBuffer> sounds;
	std::map<FontResourceType, sf::Font> fonts;
	std::map<ColorResourceType, sf::Color> colors;

public:
	static Resources& get();
	sf::SoundBuffer& sound(SoundResourceType type);
	sf::Texture& texture(TextureResourceType type);
	sf::Font& font(FontResourceType type);
	sf::Color& color(ColorResourceType type);
private:
	Resources();
	void loadAllResources();
};
