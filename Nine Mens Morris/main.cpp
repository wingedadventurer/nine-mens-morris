#include <time.h>
#include <SFML/Graphics.hpp>
#include "Game.h"
#include "Resources.h"

void instanceSingletons() {
	Resources::get();
}

int main()
{
	instanceSingletons();

	// randomize
	srand(static_cast<unsigned int>(time(NULL)));


	// init
	sf::RenderWindow window(sf::VideoMode(600, 600), "Nine Men's Morris", sf::Style::Titlebar | sf::Style::Close);
	Game game;
	sf::Event event;
	sf::Clock clock;
	bool processing = true;

	// limit fps
	window.setFramerateLimit(60);

	// main window loop
	while (window.isOpen())
	{
		// window events
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::GainedFocus) {
				processing = true;
			}
			if (event.type == sf::Event::LostFocus) {
				processing = false;
			}
		}

		// processing
		if (processing) {
			// update
			game.update(window, clock.restart().asMilliseconds());
		
			// draw
			window.clear();
			game.draw(window);
			window.display();
		}
	}

	return 0;
}