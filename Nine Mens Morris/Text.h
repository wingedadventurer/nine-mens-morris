#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include "Resources.h"

class Text
{
private:
	sf::Vector2f position;
	sf::Text text;

public:
	Text();
	Text(sf::Vector2f position, sf::String text);

	void init();
	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	void setPosition(sf::Vector2f position);
	void setText(std::string string);
	std::string getText();
	void setColor(sf::Color color);
	void setFontSize(int size);

private:
	void updateTextPlacement();
};
