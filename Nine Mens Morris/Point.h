#pragma once

#include <vector>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Resources.h"
#include "Button.h"
#include "Coin.h"

class Point : public Button
{
private:
	int id = -1;
	std::vector<Point *> connectedPoints;
	Coin *linkedCoin = nullptr;

public:
	Point(int id, sf::Vector2f position);

	void init();

	void reset();
	int getId();
	void connectTo(Point &point);
	void moveTo(sf::Vector2f position);
	void linkCoin(Coin *coin);
	void unlinkCoin();
	Coin * getLinkedCoin();
	bool hasLinkedCoin();
	bool hasFreePoint();
	void enableFreeConnectedPoints();
	void printConnections();
};
