#include "Text.h"

Text::Text()
{
	init();
}

Text::Text(sf::Vector2f position, sf::String text)
{
	init();
	setPosition(position);
	setText(text);
}

void Text::init()
{
	this->text.setFont(Resources::get().font(FontResourceType::MAIN));
	this->text.setCharacterSize(24);
}

void Text::update(sf::RenderWindow & window, int delta)
{
}

void Text::draw(sf::RenderWindow & window)
{
	window.draw(text);
}

void Text::setPosition(sf::Vector2f position)
{
	this->position = position;
	updateTextPlacement();
}

void Text::setText(std::string string)
{
	text.setString(string);
	updateTextPlacement();
}

std::string Text::getText()
{
	return text.getString().toAnsiString();
}

void Text::setColor(sf::Color color)
{
	text.setFillColor(color);
}

void Text::setFontSize(int size)
{
	text.setCharacterSize(size);
}

void Text::updateTextPlacement()
{
	sf::Vector2f textHalfSize(text.getGlobalBounds().width * 0.5f,
		text.getGlobalBounds().height * 0.5f);
	sf::Vector2f offsetFix(0.0f, text.getLocalBounds().top);
	text.setPosition(position - textHalfSize - offsetFix);
}
