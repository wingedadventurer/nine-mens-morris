#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>
#include "Resources.h"
#include "Button.h"

enum class CoinState {
	UNPLACED,
	PLACED,
	REMOVED,
};

// this is necessary to avoid recursive referencing:
// Point already #includes "Coin.h" so Coin cannon #include "Point.h".
// this way we tell Coin class that Point class exists and now we can use it
class Point;

class Coin : public Button
{
private:
	float moveLerpWeight = 10.0f;

	int playerIndex = -1;
	int n = -1;
	sf::Texture *textureNormal2;
	sf::Texture *textureHovered2;
	sf::Texture *texturePressed2;
	sf::Texture *textureDisabled2;
	sf::Vector2f homePosition;
	sf::RectangleShape rectSelected;
	sf::Vector2f targetPosition;
	CoinState coinState;
	Point *linkedPoint = nullptr;
	bool selected = false;

public:
	Coin(int n, int playerIndex, sf::Vector2f position);

	void init();
	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	void reset();
	void setPosition(sf::Vector2f position);
	void setPositionSmooth(sf::Vector2f position);
	void moveTo(sf::Vector2f position, bool smooth = true);
	void select();
	void deselect();
	bool isSelected();
	void setPlayerIndex(int playerIndex);
	int getPlayerIndex();
	void remove();
	bool isRemoved();
	void setCoinState(CoinState state);
	CoinState getCoinState();
	Point *getLinkedPoint();
	bool hasLinkedPoint();
	void linkPoint(Point *point);
	void unlinkPoint();
	void updateBackground();
};
