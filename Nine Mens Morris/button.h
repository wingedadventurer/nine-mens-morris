#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Resources.h"

enum class PressState {
	NORMAL,
	HOVERED,
	PRESSED,
	DISABLED,
};

class Button
{
protected:
	PressState pressState = PressState::NORMAL;
	
	sf::Vector2f size;
	sf::Vector2f position;
	sf::RectangleShape rect;

	// one texture for each button press state
	sf::Texture *textureNormal;
	sf::Texture *textureHovered;
	sf::Texture *texturePressed;
	sf::Texture *textureDisabled;

	bool centered = false;

	bool lastMousePressed = false;
	bool justMousePressed = false;
	bool justMouseReleased = false;

	// event-like boolean; other classes check for this variable if button is pressed
	bool justPressed = false;

public:
	Button();
	~Button();

	void update(sf::RenderWindow &window, int delta);
	void draw(sf::RenderWindow &window);

	void setPosition(sf::Vector2f position);
	sf::Vector2f getPosition();
	void setSize(sf::Vector2f size);
	sf::Vector2f getSize();
	void setTexture(sf::Texture &texture);
	virtual void updateBackground();
	void setPressState(PressState state);
	bool isMouseOver(sf::RenderWindow &window);
	bool isMousePressed();
	bool isJustPressed();
	void setCentered(bool value);
	void disable();
	void enable();
	bool isEnabled();

private:
	void updateMouseStates();
	void updatePressState(sf::RenderWindow &window);
};
